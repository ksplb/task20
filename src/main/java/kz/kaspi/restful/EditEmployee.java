package kz.kaspi.restful;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import kz.kaspi.entity.Employee;
import kz.kaspi.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

@Path("/editEmployee")
public class EditEmployee {
    @PATCH
    @Produces("application/json;charset=UTF-8")
    public Response editEmployee(Employee employee) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(employee);
        transaction.commit();
        session.close();

        String jsonAnswer = "{\"userMessage\": \"Сотрудник успешно изменен\"}";
        return Response.ok(jsonAnswer, MediaType.APPLICATION_JSON).build();
    }
}