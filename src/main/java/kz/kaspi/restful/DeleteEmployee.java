package kz.kaspi.restful;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import kz.kaspi.entity.Employee;
import kz.kaspi.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

@Path("/deleteEmployee")
public class DeleteEmployee {
    @DELETE
    @Produces("application/json")
    public Response delete(@QueryParam("id") Long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(session.get(Employee.class, id));
        transaction.commit();
        session.close();
        String jsonAnswer = "{\"userMessage\": \"Сотрудник успешно удален\"}";
        return Response.ok(jsonAnswer, MediaType.APPLICATION_JSON).build();
    }
}