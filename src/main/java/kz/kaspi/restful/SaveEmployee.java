package kz.kaspi.restful;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Response;
import kz.kaspi.entity.Employee;
import kz.kaspi.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

@Path("/saveEmployee")
public class SaveEmployee {
    @POST
    @Consumes("application/json")
    @Produces("text/plain;charset=UTF-8")
    public Response saveEmployee(Employee employee) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(employee);
        transaction.commit();
        session.close();
        return Response.ok("Сотрудник успешно сохранен").build();
    }
}