package kz.kaspi.restful;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import kz.kaspi.entity.Employee;
import kz.kaspi.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

@Path("/getEmployees")
public class GetEmployee {
    @GET
    @Produces("application/json")
    public List<Employee> getAllEmployees() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Employee> result = session.createQuery("from Employee", Employee.class).getResultList();
        session.close();
        return result;
    }

    @GET
    @Path("/searchById")
    @Produces("application/json")
    public Employee getEmployee(@QueryParam("id") Long id){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Employee employee = session.get(Employee.class, id);
        session.close();
        return employee;
    }
}